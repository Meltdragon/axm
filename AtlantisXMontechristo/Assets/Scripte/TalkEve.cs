﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TalkEve : MonoBehaviour {

	public PlayerControll s_Player;
	public GameObject g_Speechbubble;
	public int i_EveStart;
	GlobalTalkVar s_Global;
	public string s_SceneName;

	// Use this for initialization
	void Start () {

		Scene sc_CurrentScene = SceneManager.GetActiveScene ();
	    s_SceneName = sc_CurrentScene.name;
		s_Player = GameObject.FindWithTag ("Player").GetComponent<PlayerControll>();
		s_Global = GameObject.FindWithTag ("Global").GetComponent<GlobalTalkVar>();
	}

	// Update is called once per frame
	void Update () {

		if (s_SceneName == "UBoat") {
			if (s_Global.i_Speak == i_EveStart && s_Global.b_Doc == true && s_Global.b_Leader == true && s_Global.b_Sponsor == true && s_Global.b_Mechanic == true) {
				s_Global.b_EveTalk = true;
				g_Speechbubble.SetActive (true);
				s_Player.b_move = false;
			}
		} else if (s_SceneName == "Laboratory1") {
			if (s_Player.b_Enter == true) {
				if (Input.GetKey (KeyCode.E)) {
					s_Global.b_EveTalk = true;
					g_Speechbubble.SetActive (true);
					s_Player.b_move = false;
				}

			}
		}
	}
}