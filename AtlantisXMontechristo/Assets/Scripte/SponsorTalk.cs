﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SponsorTalk : MonoBehaviour {

	public PlayerControll s_Player;
	public GameObject g_Speechbubble_Sponsor;
	GlobalTalkVar s_Global;


	// Use this for initialization
	void Start () {

		s_Player = GameObject.FindWithTag ("Player").GetComponent<PlayerControll>();
		s_Global = GameObject.FindWithTag ("Global").GetComponent<GlobalTalkVar> ();

	}

	// Update is called once per frame
	void Update () {

		if (s_Player.b_Enter == true && s_Player.b_Sponsor == true) {
			if (Input.GetKey (KeyCode.E)) {
				s_Global.b_Sponsor = true;
				g_Speechbubble_Sponsor.SetActive (true);
				s_Player.b_move = false;
			}
		} 




	}

	void OnTriggerEnter2D(Collider2D other)
	{
		s_Player.b_Sponsor = true;
	}

}

