﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ink.Runtime;

public class InteractiveStory : MonoBehaviour {

    private Sprite currentSprite;
    Image currentImage;
    public GameObject textPanel;
    public GameObject imagePanel;
       
   
     [SerializeField]
    private TextAsset inkJSONAsset;
    private Story story;

    [SerializeField]
    private Canvas canvas;
        
    // UI Prefabs
    [SerializeField]
    private Text textPrefab;
    [SerializeField]
    private Button buttonPrefab;

    [SerializeField]
    private Image currentImagePrefab;

	public PlayerControll s_Player;

	public GameObject g_Speechbubble;

	LvlChange s_Lvl;
	GlobalTalkVar s_Global;
    
    void Awake()
    {
		s_Lvl = GameObject.FindWithTag ("Global").GetComponent<LvlChange> ();
		s_Player = GameObject.FindWithTag ("Player").GetComponent<PlayerControll> ();
		s_Global = GameObject.FindWithTag ("Global").GetComponent<GlobalTalkVar> ();

        Image currentImage = Instantiate(currentImagePrefab) as Image;
        currentSprite = currentImage.sprite;
		StartStory();

	}
    void StartStory()
    {
        story = new Story(inkJSONAsset.text);
        Debug.Log(story.currentText);
        ParseForTags();
        RefreshView();
        Debug.Log("Story returns");
    }

    void RefreshView()
    {
        RemoveChildren();
        CreateBackgroundImage();

        while (story.canContinue)
        {
            string text = story.Continue().Trim();
            CreateContentView(text);
        }
        

        if (story.currentChoices.Count > 0)
        {            
            for (int i = 0; i < story.currentChoices.Count; i++)
            {
                Choice choice = story.currentChoices[i];
                Button button = CreateChoiceView(choice.text.Trim());
                button.onClick.AddListener(delegate
                {
                    OnClickChoiceButton(choice);
                });
            }
        }
        else
        {
			
					Back();
            
        }
    }

    void OnClickChoiceButton(Choice choice)
    {
        story.ChooseChoiceIndex(choice.index);
        RefreshView();
    }

    void CreateContentView(string text)
    {
        Text storyText = Instantiate(textPrefab) as Text;
        storyText.text = text;
        storyText.transform.SetParent(textPanel.transform, false);
        ParseForTags();        
       
    }

    void CreateBackgroundImage()
    {
        
        currentImage = Instantiate(currentImagePrefab) as Image;
               

        if (currentSprite != null)
        {
            Debug.Log(currentSprite.ToString());
            currentImage.sprite = currentSprite;
        }
        currentImage.transform.SetParent(imagePanel.transform, true);

    }

    void ParseForTags()
    {
        List<string> tags = story.currentTags;
        Debug.Log(tags.Count.ToString());
        if (tags.Count > 0)
        {
            Debug.Log("Got tags");
            ImageContainer imageContainer = GameObject.Find("MediaContainer").GetComponentInChildren<ImageContainer>();
            string scene_image = tags[0];
            
            currentSprite = imageContainer.potential_images[IntParseFast(scene_image)];
            Debug.Log("new sprite:"+currentSprite.ToString());
            currentImage.sprite = currentSprite;
            
        }       
    }

    Button CreateChoiceView(string text)
    {
        Button choice = Instantiate(buttonPrefab) as Button;
        choice.transform.SetParent(textPanel.transform, false);

        Text choiceText = choice.GetComponentInChildren<Text>();
        choiceText.text = text;

        HorizontalLayoutGroup layoutGroup = choice.GetComponent<HorizontalLayoutGroup>();
        layoutGroup.childForceExpandHeight = false;

        return choice;
    }

    void RemoveChildren()
    {
        int childCount = textPanel.transform.childCount;
        for (int i = childCount - 1; i >= 0; --i)
        {
            GameObject.Destroy(textPanel.transform.GetChild(i).gameObject);
            
        
			if (imagePanel.transform.childCount > 0) {
				GameObject.Destroy (imagePanel.transform.GetChild (0).gameObject);
			}
		}
    }

   
    public static int IntParseFast(string value)
    {
        int result = 0;
        for (int i = 0; i < value.Length; i++)
        {
            char letter = value[i];
            result = 10 * result + (letter - 48);
        }
        return result;
    }

	void Back()
	{
		if (s_Global.b_Doc == true && s_Lvl.b_Doc == true) {
			s_Global.i_Speak += 1;
			s_Lvl.b_Doc = false;
		} else if (s_Global.b_Geo == true && s_Lvl.b_Geo == true) {
			s_Global.i_Speak += 1;
			s_Lvl.b_Geo = false;
		} else if (s_Global.b_Leader == true && s_Lvl.b_Leader == true) {
			s_Global.i_Speak += 1;
			s_Lvl.b_Leader = false;
		} else if (s_Global.b_Mechanic == true && s_Lvl.b_Mechanic == true) {
			s_Global.i_Speak += 1;
			s_Lvl.b_Mechanic = false;
		} else if (s_Global.b_Sponsor == true && s_Lvl.b_Sponsor == true) {
			s_Global.i_Speak += 1;
			s_Lvl.b_Sponsor = false;
		} else if (s_Global.b_EveTalk == true) {
			s_Global.i_Speak += 1;
			s_Global.b_EveTalk = false;
		}
		Destroy (currentImage.transform.gameObject);

		s_Player.b_move = true;
		g_Speechbubble.SetActive (false);
		StartStory ();

	}

}

