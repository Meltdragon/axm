﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Grid_Manifest : MonoBehaviour {

	public int i_GridX = 0;
	public int i_GridY = 0;
	public int i_Grid_W = 0;

	public float f_Yaxis = -0.5f;
	public float f_Xaxis = -0.5f;

	public string c_Name;

	public bool b_Activ = false;
	public bool b_Wall = false;
	public bool b_Water = false;
	public bool b_W_N = false;
	public bool b_W_E = false;
	public bool b_W_S = false;
	public bool b_W_W = false;

	public GameObject g_Ground;
	public GameObject g_Wall;
	public GameObject g_Water;
	public GameObject g_Grid;

	private int i_Grid_Rest;


	// Update is called once per frame
	void Update () {
		transform.position = new Vector2 (f_Xaxis, f_Yaxis);

		if (b_Activ == true) {
			g_Grid = Instantiate (g_Grid, new Vector2 (transform.position.x, transform.position.y), Quaternion.identity);

			if (b_Water == true && (b_W_E == true || b_W_N == true || b_W_S == true || b_W_W == true) && (i_Grid_W >= 1)) {
				if (b_W_E == true) {
					i_Grid_Rest = i_GridX - i_Grid_W;
				} else if (b_W_N == true) {
					i_Grid_Rest = i_GridY - i_Grid_W;
				} else if (b_W_S == true || b_W_W == true) {
					i_Grid_Rest = i_Grid_W;
				}
			}
			for (int x = 0; x < i_GridX; x++) {
				transform.position = new Vector2 (transform.position.x + 1, f_Yaxis);
				for (int y = 0; y < i_GridY; y++) {
					if (b_W_E == true && x >= i_Grid_Rest) {
						g_Water = Instantiate (g_Water, new Vector2 (x, y), Quaternion.identity);
						g_Water.transform.parent = g_Grid.gameObject.transform;
						NameConventionWater (x, y);
					}
					else if (b_W_N == true && y >= i_Grid_Rest) {
						g_Water = Instantiate (g_Water, new Vector2 (x, y), Quaternion.identity);
						g_Water.transform.parent = g_Grid.gameObject.transform;
						NameConventionWater (x, y);
					}
					else if (b_W_S == true && y <= i_Grid_Rest) {
						g_Water = Instantiate (g_Water, new Vector2 (x, y), Quaternion.identity);
						g_Water.transform.parent = g_Grid.gameObject.transform;
						NameConventionWater (x, y);
					}
					else if (b_W_W == true && x <= i_Grid_Rest) {
						g_Water = Instantiate (g_Water, new Vector2 (x, y), Quaternion.identity);
						g_Water.transform.parent = g_Grid.gameObject.transform;
						NameConventionWater (x, y);
					}
					else if ((x == 0 || x == i_GridX - 1 || y == 0 || y == i_GridY - 1) && b_Wall == true) {
						g_Wall = Instantiate (g_Wall, new Vector2 (x, y), Quaternion.identity);
						g_Wall.transform.parent = g_Grid.gameObject.transform;
						NameConventionWall (x, y);

					} else {
						g_Ground = Instantiate (g_Ground, new Vector2 (x, y), Quaternion.identity);
						g_Ground.transform.parent = g_Grid.gameObject.transform;
						NameConventionGround (x, y);

					}
				}
			}
			b_Activ = false;
		}




	}
	void NameConventionGround(int i_x, int i_y)
	{
		
		if (i_y == 0) {
			c_Name = "A";
		} else if(i_y == 1){
			c_Name = "B";
		}else if(i_y == 2){
			c_Name = "C";
		}else if(i_y == 3){
			c_Name = "D";
		}else if(i_y == 4){
			c_Name = "E";
		}else if(i_y == 5){
			c_Name = "F";
		}else if(i_y == 6){
			c_Name = "G";
		}else if(i_y == 7){
			c_Name = "H";
		}else if(i_y == 8){
			c_Name = "I";
		}else if(i_y == 9){
			c_Name = "J";
		}else if(i_y == 10){
			c_Name = "K";
		}else if(i_y == 11){
			c_Name = "L";
		}else if(i_y == 12){
			c_Name = "M";
		}else if(i_y == 13){
			c_Name = "N";
		}else if(i_y == 14){
			c_Name = "O";
		}else if(i_y == 15){
			c_Name = "P";
		}else if(i_y == 16){
			c_Name = "Q";
		}else if(i_y == 17){
			c_Name = "R";
		}else if(i_y == 18){
			c_Name = "S";
		}else if(i_y == 19){
			c_Name = "T";
		}else if(i_y == 20){
			c_Name = "U";
		}else if(i_y == 21){
			c_Name = "V";
		}else if(i_y == 22){
			c_Name = "W";
		}else if(i_y == 23){
			c_Name = "X";
		}else if(i_y == 24){
			c_Name = "Y";
		}else if(i_y == 25){
			c_Name = "Z";
		}

		g_Ground.name = c_Name + "_" + i_x.ToString ();


		
	}
	void NameConventionWall(int i_x, int i_y)
	{

		if (i_y == 0) {
			c_Name = "A";
		} else if (i_y == 1) {
			c_Name = "B";
		} else if (i_y == 2) {
			c_Name = "C";
		} else if (i_y == 3) {
			c_Name = "D";
		} else if (i_y == 4) {
			c_Name = "E";
		} else if (i_y == 5) {
			c_Name = "F";
		} else if (i_y == 6) {
			c_Name = "G";
		} else if (i_y == 7) {
			c_Name = "H";
		} else if (i_y == 8) {
			c_Name = "I";
		} else if (i_y == 9) {
			c_Name = "J";
		} else if (i_y == 10) {
			c_Name = "K";
		} else if (i_y == 11) {
			c_Name = "L";
		} else if (i_y == 12) {
			c_Name = "M";
		} else if (i_y == 13) {
			c_Name = "N";
		} else if (i_y == 14) {
			c_Name = "O";
		} else if (i_y == 15) {
			c_Name = "P";
		} else if (i_y == 16) {
			c_Name = "Q";
		} else if (i_y == 17) {
			c_Name = "R";
		} else if (i_y == 18) {
			c_Name = "S";
		} else if (i_y == 19) {
			c_Name = "T";
		} else if (i_y == 20) {
			c_Name = "U";
		} else if (i_y == 21) {
			c_Name = "V";
		} else if (i_y == 22) {
			c_Name = "W";
		} else if (i_y == 23) {
			c_Name = "X";
		} else if (i_y == 24) {
			c_Name = "Y";
		} else if (i_y == 25) {
			c_Name = "Z";
		}



		g_Wall.name = c_Name + "_" + i_x.ToString ();


	}
	void NameConventionWater(int i_x, int i_y)
	{

		if (i_y == 0) {
			c_Name = "A";
		} else if(i_y == 1){
			c_Name = "B";
		}else if(i_y == 2){
			c_Name = "C";
		}else if(i_y == 3){
			c_Name = "D";
		}else if(i_y == 4){
			c_Name = "E";
		}else if(i_y == 5){
			c_Name = "F";
		}else if(i_y == 6){
			c_Name = "G";
		}else if(i_y == 7){
			c_Name = "H";
		}else if(i_y == 8){
			c_Name = "I";
		}else if(i_y == 9){
			c_Name = "J";
		}else if(i_y == 10){
			c_Name = "K";
		}else if(i_y == 11){
			c_Name = "L";
		}else if(i_y == 12){
			c_Name = "M";
		}else if(i_y == 13){
			c_Name = "N";
		}else if(i_y == 14){
			c_Name = "O";
		}else if(i_y == 15){
			c_Name = "P";
		}else if(i_y == 16){
			c_Name = "Q";
		}else if(i_y == 17){
			c_Name = "R";
		}else if(i_y == 18){
			c_Name = "S";
		}else if(i_y == 19){
			c_Name = "T";
		}else if(i_y == 20){
			c_Name = "U";
		}else if(i_y == 21){
			c_Name = "V";
		}else if(i_y == 22){
			c_Name = "W";
		}else if(i_y == 23){
			c_Name = "X";
		}else if(i_y == 24){
			c_Name = "Y";
		}else if(i_y == 25){
			c_Name = "Z";
		}

		g_Water.name = c_Name + "_" + i_x.ToString ();



	}

}
