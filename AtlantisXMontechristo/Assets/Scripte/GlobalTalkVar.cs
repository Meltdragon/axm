﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalTalkVar : MonoBehaviour {

	public bool b_Doc = false;
	public bool b_Leader = false;
	public bool b_Geo = false;
	public bool b_Sponsor = false;
	public bool b_Mechanic = false;
	public bool b_EveTalk = false;
	public int i_Speak = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
