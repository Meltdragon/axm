﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Talk : MonoBehaviour {

	public PlayerControll s_Player;
	public GameObject g_Speechbubble;

	// Use this for initialization
	void Start () {

		s_Player = GameObject.FindWithTag ("Player").GetComponent<PlayerControll>();

	}
	
	// Update is called once per frame
	void Update () {
		
		if (s_Player.b_Enter == true) {
			if (Input.GetKey (KeyCode.E)) {
				g_Speechbubble.SetActive (true);
				s_Player.b_move = false;
			}
		} 

	}
}