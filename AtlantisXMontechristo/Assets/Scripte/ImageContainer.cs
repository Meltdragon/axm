﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ImageContainer : MonoBehaviour
{
    public Image currentImage;
    public Sprite[] potential_images;


    private static ImageContainer _inst;

    public static ImageContainer Instance
    {
        get
        {
            if (_inst == null)
                _inst = new ImageContainer();

            return _inst;
        }
    }
}
