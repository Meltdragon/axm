﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControll : MonoBehaviour {

	public bool b_Enter = false;
	public bool b_move = true;
	public float f_Speed = 100f;
	public Rigidbody2D r_Rigi;
	public GameObject g_Spechbubble;

	public bool b_Doc = false;
	public bool b_Leader = false;
	public bool b_Sponsor = false;
	public bool b_Geologe = false;
	public bool b_Mechanic = false;
	//public Vector2 v_body;

	// Use this for initialization
	void Start () {

		r_Rigi = GetComponent<Rigidbody2D> ();
		//v_body = this.transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (b_move == true) {
			float f_MoveX = Input.GetAxis ("Horizontal");
			float f_MoveY = Input.GetAxis ("Vertical");
			r_Rigi.velocity = new Vector2 (f_MoveX * f_Speed * Time.deltaTime, f_MoveY * f_Speed * Time.deltaTime);
		} else {
			r_Rigi.velocity = new Vector2 (0, 0);
		} 
	}

	void OnTriggerEnter2D (Collider2D other)
	{
			b_Enter = true;
			g_Spechbubble.SetActive (true);

	}

	void OnTriggerExit2D (Collider2D other)
	{
		b_Enter = false;
		g_Spechbubble.SetActive (false);
		b_Doc = false;
		b_Leader = false;
		b_Sponsor = false;
		b_Geologe = false;
		b_Mechanic = false;

	}



}
