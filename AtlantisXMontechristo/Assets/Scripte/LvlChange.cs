﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;

public class LvlChange : MonoBehaviour {

	public string s_Scene;
	public bool b_Doc = false;
	public bool b_Sponsor = false;
	public bool b_Mechanic = false;
	public bool b_Leader = false;
	public bool b_Geo = false;
	public int i_Speak = 0;
	public int i_Compensation = 0;
	GlobalTalkVar s_Global;

	// Use this for initialization
	void Start () {
		s_Global = GameObject.FindWithTag ("Global").GetComponent<GlobalTalkVar> ();
		i_Speak = i_Compensation;
		if (b_Geo == true) {
			i_Speak += 1;
		}
		if (b_Doc == true) {
			i_Speak += 1;
		}
		if (b_Leader == true) {
			i_Speak += 1;
		}
		if (b_Sponsor == true) {
			i_Speak += 1;
		}
		if (b_Mechanic == true) {
			i_Speak += 1;
		}


	}
	
	// Update is called once per frame
	void Update () {



		if (i_Speak <= s_Global.i_Speak) {
			EditorSceneManager.LoadScene (s_Scene);

		}


	}

}
