Talk to Francois ?

*[Yes] -> talk
*[No] -> DONE
#0

=== talk
Oh you are finally awake ? You missed the landing ! 
But it was just as you suggested: The magnetic field we found belongs to this protective bubble that is shielding us from the water! -> Maindialogue

=== Maindialogue
Atlantis - the sunken island - can you believe it ?

* [How can we breathe here ?] We think the bubble might be able to extract the oxygen from the water surrounding us. It also seems to send out the carbondioxide as you can see in those little bubbles leaving to the surface. It's like a miracle, but the air in here is even more pure than i could ever imagine ! -> Maindialogue
* [It's not thaaaat special.] Fool! Scientists from all over the world found hints and "proof" of Atlantis actually existing, but noone ever came close to pinpointing it's location.
This place has been searched for since the end of the middle ages! Maybe even before that. 
What we did will be going into the history books of future generations as the most important discovery in human history! -> Maindialogue
* [What should our next steps be?] -> nextsteps

=== nextsteps

Hmm I already sent -Geologist- and -Sponsor- to explore the buildings surrounding us. 
-Mechanic- is setting up our basecamp right now and the doc is unloading his tools.
So I think it would be best to have you scouting around the island itself ! Keep your eyes open for special looking places and relics that could help us understand these ancient technologies used here.

* [Exploring on my own ?] Yes I am aware this is risky, but so far we have not met anyone so it should be safe to say this island is abandoned. Don't worry there shouldn't be any danger here. -> endchoice
* [Alright, I thought the same] Right? See, that is why you are the one that is going to take my place after this. Now go and find whatever this island has in store for us! -> endchoice

=== endchoice

* [Leave] -> DONE
