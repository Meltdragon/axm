
#0
* Begin Story ->Left_behind 

//->second_day_in_forest

//->rest_of_the_day

===Left_behind
#1
The sun is going down, and so is the fire our parents made for us. Hansel is standing in front of it, jumping from one leg on to the other.
Sitting on the ground for hours, I am starting to get cold and wonder, if I shouldn't stand up and move a little as well.
Suddenly Hansel stops and turns towards me. 
“They are coming back this time, right Gretel? It's not like last time. They won't leave us here, will they?" he asks.
*   [Tell him the truth.] "Hansel, you know the answer. You even prepared for it. I mean, why did you leave morsels on the path if you really believed they'll come back?" I say honestly. ->what_now  
*   [Calm him down.]”I am sure they will. Why don't you put some more branches on the fire. It's starting to go out." I say, well aware that eventually he needs to face the truth.-> kill_time 
*    [Shrug.]"Hansel, maybe they will, maybe they won't. Probably too soon to be sure?" I answer indecisively, well aware of the bitter truth.->kill_time 
==what_now
#2
"So what do we do now?" he asks. 
*   [“You know what.”]"Same as last time" I say, "we wait for the moon to rise and then follow your trail back home." ->break_up
==kill_time
#3
We keep waiting a little longer. Eventually Hansel sits down, tears in his eyes he says quietly:
 “I know they're not coming back." #1
I don't know what to say. He is right. ->what_now
==break_up
One hour later the moon is high up on the nightly sky. We are lucky, since it's almost full, the night is brightly lit. #3
Hansel runs away from the fireplace shouting: "I remember throwing the last morsels down here." Before you can react he is out of sight.
*   [I ask him to wait.]"Hey Hansel don't you dare leaving me here!" I shout, his voice comes from just a few meters down "I am right here."->no_breadcrumbs
*   [I run after him as fast as I can.]->no_breadcrumbs

*   [I follow him calmly.]->no_breadcrumbs

    =no_breadcrumbs
    I see Hansel's silhouette against the moonlight. He is standing and looking at the ground, searching for something.#4
    "This is wrong." he says, "I am absolutely sure, that I threw some breadcrumbs here. I remember them landing on that old tree chunk."
    * [I help him looking.] "I help you." I say and get closer, scanning the ground for crumbs of the bread we brought this morning and that Hansel had used to mark the path home. ->Getting_lost
    * [I advice to look again.]"If you are so sure, you should look again?" I say. ->Getting_lost
    * [I doubt him.] "You are probably looking at the wrong place. Are you really sure?" I say, trying to sound not too patronizing. 
    "This is the place." he insists. "I am really sure."->Getting_lost
    

===Getting_lost
We continue to search the place. {We|Once more we|And again we} go further into the direction where Hansel thinks we had come from and since we don't find anything, we go back to the place where we had rested, investigating every centimeter on the way.
{No|Still no|But no matter how hard we look, there just is no} sign of breadcrumbs.

*   [Continue looking.] ->Getting_lost
*   [Blame Hansel.] "You must remember it wrong." I accuse him, "it's probably in a different direction."
    "If you're so sure about that, why don't you look in different directions." he responds.
        **  [That's what I'll do.] "That is exactly what I will do now." I say and start searching in circles around the fireplace.
            But I have no luck. Eventually I give up and join Hansel.->Getting_lost
        ** [Let's do it together.] "Come on Hansel. Don't be upset, let's try searching together." I say. 
            "If it makes you happy," he says, "I tell you, I left them down at that tree chunk."
            We start searching in circles around the fireplace. But we have no luck and eventually return to the original spot.->Getting_lost
        **  [Rather not.]"It's okay" I say, "I believe you. Let's look where you suggested." ->Getting_lost
    
*   [Give up.] Frustrated by the unsuccessful  search we give up.
    "They are all gone," says Hansel, a tremble in his voice, "the birds must have eaten them. I am so stupid! What was I thinking. 
    I am sorry Gretel, we will never make it home, and it's all my fault!"
        **  ["I agree, this is terrible."] "Well, yes," I say, "using breadcrumbs was indeed not very clever. Making it back without a trail, will be very hard." ->begin_journey
        **  ["What happened, happened. We need to make the best out of it."] "Now it happened," I say, "and blaming yourself doesn't help."->begin_journey
        **  ["Don't be ridicolous, this is not your fault."]"Don't be ridicolous, this is not your fault. We will make it home. Somehow." I say.->begin_journey
    
    
    ==begin_journey
    "Be it as it is," Hansel states "we need to make it back. So let's try it. How about we go into the direction of the tree chunk. We came from there this morning."
    *   [I agree.] "Seems like the best choice to me." I respond. ->move_east
    *   [I disagree.] "So far your choices were pretty poor. I say, we go into the opposite direction." I argue.
        "I believe that's the opposite from the right direction, but if you insist to wrong me, so be it." says Hansel.->move_west
    *   [Shrug.] "If you say so, then lead the way" I say resignedly and follow Hansel down the path.
    
    ==move_east //follow hansel and move towards the moon
    We walk towards the moon, the direction Hansel picked. The forest gets thicker and the path becomes more difficult the further we move. Eventually the trees are so dense, that we lose sight of the moon.
    I ask my brother
    *   [ how he can orientate without the moon.]"Hansel, how do you know, we are still moving into the right direction, with the moon being gone?"->take_a_break
    *   [ if we shouldn't stop and wait for the sun]"Hansel, How about we take a break and wait for the sun coming up again?"->take_a_break
    *   [ if he is alright.]"Hansel, you good? It's pretty dark and the forest here is very thick."->take_a_break
        
        =take_a_break
        We decide to take a break and wait for the sun to rise in the morning. I am so exhausted, that I fall asleep, as soon as my body touches the ground.
        *   [Sleep until morning.]->second_day_in_forest
        
    ==move_west //don't follow hansel and walk away from the moon
    Ignoring his complains, I lead the way and we move having the moon in our backs. The forest gets lighter and it seems like we might go into the right direction. 
    *   [Brag about it]"See? I told you it was this direction." I tell my brother.->reach_lake
    *   [Be self-effacing]"This could be the right way. What do you think Hansel?" I ask my brother.->reach_lake
    *   [Continue quitely]->reach_lake
    
        =reach_lake
        After a longer walk we finally reach the lengthy shore of a big lake. The moonlight is reflected by the water. I realize, that I must have picked the wrong direction.  I don't remember us passing a lake when we went into the woods this morning and this one is way too big to miss it.
        
        *   [Blame Hansel] "Great. So this actually is the wrong direction. Thank you brother, now we need to walk all the way back. Hope it feels good to be right." I complain, while turning around from the lake.
            He looks at me flabbergasted.->move_east
        *   [Admit mistake] "That was my bad. I am sorry. I should have listened to you Hansel." I say.
            He nods and responds: "It's fine Gretel. Let's just move now"->move_east
        *   [Turn around silently] "Gretel?" I hear Hansel's voice when I turn around, "Are we now going in the direction I proposed after all?" he asks.
            "Yes." I hiss, "we go into the direction you proposed after all".->move_east
    
===second_day_in_forest
I sleep deep as a stone all night without dreams or interruptions. When the day breaks, Hansel wakes me up. 
"Get up!" he says, "we need to get moving."
*   [Get up] ->getting_up
*   [Ask for a bit more time]"Just a little longer." I say and try to turn on the other side, but he is persistent and pulls my arm. I have no choice but to give in. ->getting_up 
*   [Ignore him] I pretend I haven't heard him and turn on the other side, but he is persistent and pulls my arm. I have no choice but to give in. ->getting_up 

    ==getting_up
    "Alright, alright." I say and get up from my nightly camp. Stretching my limbs and yawning, I realize that while I have slightly more energy than last night, I am still very exhausted and extremely hungry.
    *   [We should try to find some food around here] "Hansel, I am starving and I guess you aren't much better. We should search the area close by for something to eat." I say.
        "You are right." ->searching_for_food 
    *   [We should try to get home as fast as possible]"I guess you are just as hungry as I am, but I still think we shouldn't waste any time and find the way home right away." I insist.->searching_for_food 
    *   [Ask Hansel what he thinks we should do]"I am hungry, but I also think we should hurry getting home. What do you think?"->searching_for_food 

    ==searching_for_food
    Hansel looks at me and says:"There is no point in moving on before eating. We are to weak already. Let's search the area and see if we can find anything here.
    *   [Search the bushes for berries]->berries
    *   [Watch out for small animals]->animals
    *   [Look at the trees for mushrooms]->mushrooms
    
    ==berries
    I walk{| further| even further| really far} around and look at each and every bush, hoping to see the red or blue glimpse of berries.
        {->berries_fail|->berries_fail|->berries_fail|->berries_success}
    
        =berries_fail
        Holding up branches and pulling away leaves,{| cutting myself at spikey thorns,| crawling under the bushes to see if something is left there,} yet there are no berries.
        +   [Continue looking for berries] {There must be berries somewhere, so I continue looking.| I don't give up that easily.| This forest is full of berries and I will find them.}->berries
        *   [Watch out for animals]->animals
        *   [Look at the trees for mushrooms]->mushrooms
        
        =berries_success
        Finally I find a couple of bushes that carry a handfull of the precious red and blue fruit. Finally! I pick all of them. 
        *   [Return to the Hansel]->food
    
    
    ==animals
    {I grab a heavy, sharp stone from the ground and move away from our resting place.| I pick up my stone and try once more to hunt.} Cautiously avoiding noise, I sneak around watching out for movement of anything small and careless enough to become our breakfast.
        +  [Hunt] {But it is strange. While I know the forest normally to be full of small animals, squirrells, mice, dax - here and today there is nothing.|But no matter how hard I look and listen - still no sign of life, other than my little brother.}
            I don't even hear a bird singing.
            ++   [Search the bushes for berries]->berries
            **   [Look at the trees for mushrooms]->mushrooms
        
    
        
    ==mushrooms
   {From past experience I know which trees are likely to have mushrooms nearby, so I start my search at oaks and beeches.| Maybe I overlooked some oaks and beeches the first time, so I go again looking for mushrooms.}
    +   [Find mushrooms]{But it seems to be the wrong time of the year. I remember father telling me, that mushrooms needed rain to grow - and the last weeks had been dry. So it seems like I won't have any luck today.| Still no mushrooms. Either I have really bad luck, or there just aren't any at this time.}
        ++   [Search the bushes for berries]->berries
        **   [Watch out for small animals]->animals
    
    ==food
    When I meet up with Hansel, we eat half of the berries and I take the rest under my apron for later. This meal wasn't enough to stop me being hungry but at least I feel now strong enough to continue our journey.
    *   [Continue]->rest_of_the_day
    
    ==rest_of_the_day
    We continue our search for the way home through the seemingly infinite forest. We don't speak a word, saving our energy for the walk. Neither of us knows if we are on the right track. 
    *   [Keep moving.]->march_on
    
        =march_on
        {Since the trees are so dense in this area, we can't even tell if we have been walking into the right direction  during the last couple of hours.| My feet are hurting and the painful feeling of hunger and weakness that had been gone after I had eaten the berries returned worse than ever.|Every step hurts and wave of cramps float from the stomach through my entire body.|Can't...walk...any...further...->propose_rest} 
        +   {TURNS_SINCE(-> rest_of_the_day) < 4}[March on.]->march_on
        +   {TURNS_SINCE(-> rest_of_the_day) == 3}[Look at Hansel]->look_at_hansel
       
        =look_at_hansel
        I look at my brother and he appears even worse than I feel. His eyes are sunken in, his steps are slow and each time a foot touches the ground he groans quietly.
        +   [Forward.] ->march_on
        +   [Propose to rest]"Let's just rest here Hansel. I am done and so are you."->propose_rest
        
        =propose_rest
        Exhausted and with no energy left I lean my back against a tree and sink to the ground. Through the leaves I see that the light is fading - it's getting night.
        *   [Sleep right away] I close my eyes and the next moment I fall asleep. ->sleep
        *   [Eat the rest of the berries] ->eat_the_last_berries
        
        =sleep
       {eat_the_last_berries: After some deep and dreamless rest, the daybreak wakes me with light.  ->third_day_in_forest  | Short time later I wake up, from hurting cramps of hunger that shake my body. I have no choice, I must eat. I wake up Hansel: "Let's eat the berries. We will find more tomorrow" I say to him.->eat_the_last_berries}
        
        =eat_the_last_berries
        We share the last berries. It's not much, and we are out of food again, but at least the worst hunger is gone for now.
        [Sleep]->sleep
        
    ===third_day_in_forest
    I can tell, that it must be close to sunrise. I look at Hansel, he is still sleeping. # speaker:audiofile:line
    * wake Hansel up ->wake_hansel_up
    * look for food alone
    * sleep a bit longer 
    
        ==wake_hansel_up
        
    
    
    
        ->DONE
        
        
    
    
    
    

    
    





